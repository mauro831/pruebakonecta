<?php
	require_once ("../config/db.php");
	require_once ("../config/conexion.php");
	include("../funciones.php");
	
	$action = (isset($_REQUEST['action'])&& $_REQUEST['action'] !=NULL)?$_REQUEST['action']:'';
	if (isset($_GET['id'])){
		$id_producto=intval($_GET['id']);
		$delete1=mysqli_query($con,"DELETE FROM productos WHERE id='".$id_producto."'");
	}	
	if($action == 'ajax'){
		// escaping, additionally removing everything that could be (html/javascript-) code
         $q = mysqli_real_escape_string($con,(strip_tags($_REQUEST['q'], ENT_QUOTES)));
		 $id_categoria =intval($_REQUEST['id']);
		 $aColumns = array('id', 'nombre');//Columnas de busqueda
		 $sTable = "productos";

		include 'pagination.php';
		//pagination variables
		$page = (isset($_REQUEST['page']) && !empty($_REQUEST['page']))?$_REQUEST['page']:1;
		$per_page = 18; //how much records you want to show
		$adjacents  = 4; //gap between pages after number of adjacents
		$offset = ($page - 1) * $per_page;
		//Count the total number of row in your table*/
		$count_query   = mysqli_query($con, "SELECT count(*) AS numrows FROM $sTable  $sWhere");
		$row= mysqli_fetch_array($count_query);
		$numrows = $row['numrows'];
		$total_pages = ceil($numrows/$per_page);
		$reload = './productos.php';
		//main query to fetch the data
		$sql="SELECT * FROM  $sTable $sWhere LIMIT $offset,$per_page";
		$query = mysqli_query($con, $sql);
		//loop through fetched data
		if ($numrows>0){
			
			?>
			  
				<?php
				$nums=1;
				while ($row=mysqli_fetch_array($query)){
						$id_producto=$row['id'];
						$codigo_producto=$row['referencia'];
						$nombre_producto=$row['nombre'];
						$stock=$row['stock'];
						$precio=$row['precio'];
					?>
					
					<div class="col-lg-2 col-md-2 col-sm-6 col-xs-12 thumb text-center ng-scope" ng-repeat="item in records">
						  <a class="thumbnail" href="producto.php?id=<?php echo $id_producto;?>">
							  <span title="Current quantity" class="badge badge-default stock-counter ng-binding">Cantidad: <?php echo number_format($stock,2); ?></span>
							  <span title="Low stock" class="low-stock-alert ng-hide" ng-show="item.current_quantity <= item.low_stock_threshold"><i class="fa fa-exclamation-triangle"></i></span>
							 <hr>
							  <span class="thumb-name"><strong><?php echo $nombre_producto;?></strong></span>
						      <span class="thumb-code ng-binding">Referencia: <?php echo $codigo_producto;?></span>
						      <span class="thumb-name">$<?php echo $precio;?></span>
						  </a>

					</div>
					<?php
					if ($nums%6==0){
						echo "<div class='clearfix'></div>";
					}
					$nums++;
				}
				?>
				<div class="clearfix"></div>
				<div class='row text-center'>
					<div ><?php
					 echo paginate($reload, $page, $total_pages, $adjacents);
					?></div>
				</div>
			
			<?php
		}
	}
?>
