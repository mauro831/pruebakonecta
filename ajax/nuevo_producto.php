<?php
	require_once ("../config/db.php");
	require_once ("../config/conexion.php");
	include("../funciones.php");
		// escaping, additionally removing everything that could be (html/javascript-) code
		$nombre=mysqli_real_escape_string($con,(strip_tags($_POST["nombre"],ENT_QUOTES)));
		$referencia=mysqli_real_escape_string($con,(strip_tags($_POST["referencia"],ENT_QUOTES)));
		$precio=intval($_POST['precio']);
		$peso=intval($_POST['peso']);
		$categoria=mysqli_real_escape_string($con,(strip_tags($_POST["categoria"],ENT_QUOTES)));
		$stock=intval($_POST['stock']);
		$fecha=date("Y-m-d H:i:s");
		
		$sql="INSERT INTO productos (nombre, referencia, precio, peso, categoria, stock, fecha) VALUES ('$nombre', '$referencia', '$precio', '$peso', '$categoria', '$stock', '$fecha')";
		//echo 'La consulta a ejecutar es '.$sql;
		$query_new_insert = mysqli_query($con,$sql);
			if ($query_new_insert){
				$messages[] = "Producto ha sido ingresado satisfactoriamente.";
				$id_producto=get_row('productos','id', 'nombre', $referencia);

			} else{
				$errors []= "Lo siento algo ha salido mal intenta nuevamente.".mysqli_error($con);
			}

		
		if (isset($errors)){
			
			?>
			<div class="alert alert-danger" role="alert">
				<button type="button" class="close" data-dismiss="alert">&times;</button>
					<strong>Error!</strong> 
					<?php
						foreach ($errors as $error) {
								echo $error;
							}
						?>
			</div>
			<?php
			}
			if (isset($messages)){
				
				?>
				<div class="alert alert-success" role="alert">
						<button type="button" class="close" data-dismiss="alert">&times;</button>
						<strong>¡Bien hecho!</strong>
						<?php
							foreach ($messages as $message) {
									echo $message;
								}
							?>
				</div>
				<?php
			}

?>