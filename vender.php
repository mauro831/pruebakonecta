<?php
	/* Connect To Database*/
	require_once ("config/db.php");
	require_once ("config/conexion.php");
	
	$title="Venta";
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <?php include("head.php");?>
  </head>
  <body>
	<?php
	include("navbar.php");
	?>
	
	<div class="container content-invoice">
	    <h2 class="title">Venta</h2>
			<span class="item-title">Buscar referencia del producto: </span>
		    <input type="text" name="busqueda" id="busqueda" value="" placeholder="" maxlength="30" autocomplete="off"/>
		<div id="resultadoBusqueda"></div>		
	</div>
	<hr>
	<div class="container">		
		<h2 class="title">Listado</h2>	
		<?php		  
			$sql="select pv.id, p.nombre, pv.cantidad_vendida from productos_vendidos as pv JOIN productos as p on p.referencia = pv.producto_id ";
		    $query=mysqli_query($con,$sql);

			while($rows=mysqli_fetch_array($query)){

				$Pid = $rows['id'];
				$nom = $rows['nombre'];
				$cant = $rows['cantidad_vendida'];
				?>
				<table class="table table-bordered table-hover" id="invoiceItem">	
					<tr>
						<th width="15%">Nro: <?php echo $Pid?></th>
						<th width="20%">Producto: <?php echo $nom?></th>
						<th width="15%">Cantidad a vender: <?php echo $cant?></th>							
					</tr>
				</table>
				<?php
			}
		?>
    </div>	
	<?php
	   include("footer.php");
	?>
  </body>
</html>
<script>
 $(document).ready(function(){
	 
	$("#resultadoBusqueda").html('<p>Consulta vacia</p>');
	$('#busqueda').on('keyup', function(){
		var search = $("#busqueda").val();
		if (search != "") {
			$.post("ajax/buscar_referencia.php", {valorBusqueda: search}, function(datos) {
				$("#resultadoBusqueda").html(datos);
			}); 
		} else { 
			$("#resultadoBusqueda").html('<p>Consulta vacia</p>');
		};
    });

	function Registrar(){
		var cantidad = $('#cantidad_2').val();
		var referencia = $("#busqueda").val();

		$.ajax({
			type: "POST",
			url: "ajax/cantidad_vendida.php",
			data: "cant="+cantidad+"&ref="+referencia,
			success: function(datos){
              console.log(datos);
			}
		});
    }

});	
</script>