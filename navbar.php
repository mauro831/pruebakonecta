	<?php
		if (isset($title))
		{
	?>
    <nav class="navbar navbar-default ">
      <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">Inventario</a>
        </div>
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
          <ul class="nav navbar-nav">
            <li class="<?php if (isset($active_productos)){echo $active_productos;}?>"><a href="inventario.php"><i class='glyphicon glyphicon-barcode'></i> Inventario</a></li>
            <li class="<?php if (isset($active_vender)){echo $active_vender;}?>"><a href="vender.php"><i class='glyphicon glyphicon-barcode'></i> Vender</a></li>
          </ul>
        </div><!-- /.navbar-collapse -->
      </div><!-- /.container-fluid -->
    </nav>
	<?php
		}
	?>