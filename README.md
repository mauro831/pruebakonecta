
En carpeta db esta el archivo de la base de datos
En la modulo de vender se busca la referencia del producto y se ingresar la cantidad a vender y se guarda en la base de datos

Realizar una consulta que permita conocer cuál es el producto que más stock tiene.

SELECT * 
FROM productos as p 
WHERE p.stock < (
    SELECT SUM(b.stock) FROM productos as b WHERE b.stock < p.stock
) 

Realizar una consulta que permita conocer cuál es el producto más vendido.

SELECT pv.producto_id, SUM(pv.cantidad_vendida) AS total 
FROM productos_vendidos as pv 
GROUP BY pv.producto_id 
ORDER BY SUM(pv.cantidad_vendida) DESC 
