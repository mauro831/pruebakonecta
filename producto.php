<?php
	require_once ("config/db.php");
	require_once ("config/conexion.php");
	include("funciones.php");
	
	$title="Producto";

	if (isset($_GET['id'])){
		$id_producto=intval($_GET['id']);
		$query=mysqli_query($con,"select * from productos where id='$id_producto'");
		$row=mysqli_fetch_array($query);
		
	} else {
		die("Producto no existe");
	}

	if (isset($_GET['id'])){
		$id=intval($_GET['id']);
		$delete1=mysqli_query($con,"DELETE FROM productos WHERE id='".$id."'");
	}
	
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <?php include("head.php");?>
  </head>
  <body>
	<?php
	include("navbar.php");
	include("modal/editar_productos.php");

	?>
	
	<div class="container">

<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
          <div class="panel-body">
            <div class="row">
				<div class="col-sm-6 text-center">
					<div class="row margin-btm-20">
						<div class="col-sm-12">
						<span class="item-title">Producto: <?php echo $row['nombre'];?></span>
						</div>
						<div class="col-sm-12 margin-btm-10">
						<span class="item-number">Referencia: <?php echo $row['referencia'];?></span>
						</div>
						<div class="col-sm-12 margin-btm-10">
						</div>
						<div class="col-sm-12">
						<span class="current-stock">Stock disponible</span>
						</div>
						<div class="col-sm-12 margin-btm-10">
						<span class="item-quantity"><?php echo number_format($row['stock']);?></span>
						</div>
						<div class="col-sm-12">
						<span class="current-stock">Precio venta</span>
						</div>
						<div class="col-sm-12">
						<span class="item-price">$ <?php echo number_format($row['precio']);?></span>
						</div>
						<div class="col-sm-12 margin-btm-10">
						</div>
						<div class="col-sm-6 col-xs-6 col-md-4 ">
						
						</div>
						<div class="col-sm-12 margin-btm-10">
						</div>
					</div>
				</div>
				<div class="col-sm-6 text-center">
				    <a href="#myModal2" data-toggle="modal" data-nombre='<?php echo $row['nombre'];?>' data-referencia='<?php echo $row['referencia'];?>' data-categoria='<?php echo $row['categoria']?>' data-precio='<?php echo $row['precio']?>' data-stock='<?php echo $row['stock'];?>' data-id='<?php echo $row['id'];?>' class="btn btn-info" title="Editar"> <i class="glyphicon glyphicon-pencil"></i> Editar </a>
					<a href="" class="btn btn-danger" onclick="eliminar('<?php echo $row['id'];?>')" title="Eliminar"> <i class="glyphicon glyphicon-trash"></i> Eliminar </a> 		
				</div>
            </div>
            <br>
			<div class="row">
				<div class="col-sm-12 text-center">
				<button type="button" onclick="location.href='inventario.php'">Regresar al inventario</button>
				</div>
            </div>
            <div class="row">

            <div class="col-sm-8 col-sm-offset-2 text-left">
                  <div class="row">
                    <?php
						if (isset($message)){
							?>
						<div class="alert alert-success alert-dismissible" role="alert">
						  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						  <strong>Aviso!</strong> Datos procesados exitosamente.
						</div>	
							<?php
						}
						if (isset($error)){
							?>
						<div class="alert alert-danger alert-dismissible" role="alert">
						  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						  <strong>Error!</strong> No se pudo procesar los datos.
						</div>	
							<?php
						}
					?>	
                  </div>   
				</div>
            </div>
          </div>
        </div>
    </div>
</div>



</div>

	
	<?php
	include("footer.php");
	?>
	<script type="text/javascript" src="js/productos.js"></script>
  </body>
</html>
<script>
$( "#editar_producto" ).submit(function( event ) {
	event.preventDefault();
    $('#actualizar_datos').attr("disabled", true);
    var parametros = $(this).serialize();
	 $.ajax({
			type: "POST",
			url: "ajax/editar_producto.php",
			data: parametros,
			 beforeSend: function(objeto){
				$("#resultados_ajax2").html("Mensaje: Cargando...");
			  },
			success: function(datos){
			$("#resultados_ajax2").html(datos);
			$('#actualizar_datos').attr("disabled", false);
			window.setTimeout(function() {
				$(".alert").fadeTo(500, 0).slideUp(500, function(){
				$(this).remove();});
				location.replace('inventario.php');
			}, 7000);
		  }
	});
})

	$('#myModal2').on('show.bs.modal', function (event) {
		var button = $(event.relatedTarget)
		var nombre = button.data('nombre')
		var referencia = button.data('referencia')
		var categoria = button.data('categoria')
		var precio = button.data('precio')
		var stock = button.data('stock')
		var id = button.data('id')
		var modal = $(this)
		modal.find('.modal-body #nombre').val(nombre)
		modal.find('.modal-body #categoria').val(categoria)
		modal.find('.modal-body #referencia').val(referencia)
		modal.find('.modal-body #precio').val(precio)
		modal.find('.modal-body #stock').val(stock)
		modal.find('.modal-body #id').val(id)
	})
	
	function eliminar(id){
		//var q= $("#q").val();
		if (confirm("Realmente deseas eliminar el producto")){	
			//location.replace('inventario.php?delete='+id);
			location.href('inventario.php');

		}
	}
</script>